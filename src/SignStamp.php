<?php

namespace Thiagoprz\SignStamp;

use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Http;
use Thiagoprz\SignStamp\Exception\SignStampErrorException;
use Thiagoprz\SignStamp\Http\Client;
use Thiagoprz\SignStamp\Models\Document;
use Thiagoprz\SignStamp\Models\Envelope;
use Thiagoprz\SignStamp\Models\Signature;
use Thiagoprz\SignStamp\Services\Signature\Signatory;

class SignStamp
{
    /**
     * @param $ref
     * @param $subject
     * @param Document[]|array $documents
     * @param Signatory[]|array $signatories
     */
    public static function sign($ref, $subject, array $documents, array $signatories)
    {
        // Cria o envelope
        /* @var Envelope $envelope */
        $envelope = Envelope::create(compact('ref', 'subject'));

        /* @var Document[] $files */
        $files = [];

        // Cria os documentos
        foreach ($documents as $document) {
            $files[] = Document::create([
                'path' => $document,
                'title' => $subject,
                'summary' => $subject,
                'envelope_uuid' => $envelope->uuid,
            ]);
        }

        // Assina o envelope
        foreach ($signatories as $signatory) {
            if (is_array($signatory)) {
                $signature = new Signature();
                $signature->signatory_uid = $signatory['signatory_uid'];
                $signature->name = $signatory['name'];
                $signature->email = $signatory['email'];
                $signature->cellphone = $signatory['cellphone'];
                $signature->envelope_uuid = $envelope->uuid;
                $signature->keyword = $signatory['keyword'];
                $signature->addDocuments($files);
                $signature->save();
            } else {
                $signature = new Signature();
                $signature->signatory_uid = $signatory->getUuid();
                $signature->name = $signatory->getName();
                $signature->email = $signatory->getEmail();
                $signature->cellphone = $signatory->getCellphone();
                $signature->envelope_uuid = $envelope->uuid;
                $signature->keyword = $signatory->getKeyword();
                $signature->addDocuments($files);
                $signature->save();
            }
        }

//        foreach ($files as $file) {
//            $destiny = config('signstamp.overwrite') ? $file->path : str_replace('.pdf', '_assinado.pdf', $file->path);
//            \Thiagoprz\SignStamp\Services\Document::download($file->envelope_uuid, $file->uuid, $destiny);
//        }

        return Services\Envelope::show($envelope->uuid);
    }


    /**
     * @param string $file
     * @param string $title
     * @param string $summary
     * @param mixed|Signatory $signatory
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function quickSign($file, $title, $summary, $signatory)
    {
        $documentFile = fopen($file, 'r');
        $client = new Client();
        $client->setAccessToken("Basic " . base64_encode(config('signstamp.api.username') . ':' . config('signstamp.api.password')));
        try {
            $quickSignRequest = [
                'documentInformation' => [
                    'title' => $title,
                    'summary' => $summary,
                ],
                'signatureSetup' => [
                    'signatory' => $signatory,
                    'signatureStampConfig' => [
                        'allPagesSettings' => [
                            'placeHolderType' => 'keyword',
                            'keyword' => $signatory->getKeyword(),
                            'width' => 200,
                            'height' => 40,
                            'showEvidences' => true,
                        ],
                    ],
                ],
                'signatureEvidences' => [
                    'thumbnailImageData' => self::generatePNG($signatory->getName()),
                ],
            ];

            $data = Http::withHeaders([
                'Authorization' => 'Basic ' . base64_encode(config('signstamp.api.username') . ':' . config('signstamp.api.password')),
            ])
                ->timeout(120)
                ->attach('documentFile', file_get_contents($file), basename($file))
                ->post('https://quicksign.pluriosign.com.br/assinar', [
                    [
                        'name' => 'quickSignRequest',
                        'contents' => json_encode($quickSignRequest),
                    ]
                ]);
            $destiny = config('signstamp.overwrite') ? $file : str_replace('.pdf', '_assinado.pdf', $file);
            file_put_contents($destiny, (string)$data->getBody());
        } catch(ClientException $exception) {
            throw new SignStampErrorException((string)$exception->getResponse()->getBody(), 500, config('signstamp.api.baseUrl') . '/assinar');
        }
        return $destiny;
    }


    public static function generatePNG($name)
    {
        $font = 25;
        $string = ' ';
        $im = @imagecreatetruecolor(strlen($string) * $font / 1.5, $font);
        imagesavealpha($im, true);
        imagealphablending($im, false);
        $white = imagecolorallocatealpha($im, 255, 255, 255, 127);
        imagefill($im, 0, 0, $white);
        $black = imagecolorallocate($im, 0, 0, 0);
        imagestring($im, $font, 0, 0, $string, $black);
        header("Content-type: image/png");
        ob_start ();
        imagepng($im);
        $image_data = ob_get_contents ();
        ob_end_clean ();
        return base64_encode($image_data);
    }
}
