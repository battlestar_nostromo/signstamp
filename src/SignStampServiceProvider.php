<?php

namespace Thiagoprz\SignStamp;

use Illuminate\Support\ServiceProvider;

/**
 * @package Thiagoprz\SignStamp
 */
class SignStampServiceProvider extends ServiceProvider
{

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        // Loading migration
        $this->loadMigrationsFrom(__DIR__.'/migrations');
        // Publishes configuration
        $this->publishes([
            __DIR__.'/config/signstamp.php' => config_path('signstamp.php'),
        ]);
        // i18n
        $this->loadTranslationsFrom(__DIR__.'/lang/', 'signstamp');
    }

}
