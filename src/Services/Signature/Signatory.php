<?php

namespace Thiagoprz\SignStamp\Services\Signature;

/**
 * @package Thiagoprz\SignStamp\Services\Signature;
 */
class Signatory
{
    /**
     * @var string
     */
    public $uid;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $email;

    /**
     * @var string
     */
    public $cellphone;
    protected $keyword;
    public $identityConfirmationPolicy;

    /**
     * @param string $uid
     * @param string $name
     * @param string $email
     * @param string $cellphone
     */
    public function __construct($uid, $name, $email, $cellphone, $keyword, $identityConfirmationPolicy = 0)
    {
        $this->uid = $uid;
        $this->name = $name;
        $this->email = $email;
        $this->cellphone = str_replace(['.', '-', '(', ')', '#', ' '], '', $cellphone);
        $this->keyword = $keyword;
        $this->identityConfirmationPolicy = $identityConfirmationPolicy;
    }

    /**
     * @return string
     */
    public function getUuid(): string
    {
        return $this->uuid;
    }

    /**
     * @param string $uuid
     */
    public function setUuid(string $uuid): void
    {
        $this->uuid = $uuid;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getCellphone(): string
    {
        return $this->cellphone;
    }

    /**
     * @param string $cellphone
     */
    public function setCellphone(string $cellphone): void
    {
        $this->cellphone = $cellphone;
    }

    /**
     * @return mixed
     */
    public function getKeyword()
    {
        return $this->keyword;
    }

    /**
     * @param mixed $keyword
     */
    public function setKeyword($keyword): void
    {
        $this->keyword = $keyword;
    }

}
