<?php

namespace Thiagoprz\SignStamp\Services\Signature\SignatureStampConfig;

/**
 * @package Thiagoprz\SignStamp\Services\Signature\SignatureStampConfig
 */
class AllPagesSettings
{
    /**
     * @var string
     */
    public $placeHolderType;

    /**
     * @var string
     */
    public $keyword;

    /**
     * @var int
     */
    public $width;

    /**
     * @var int
     */
    public $height;

    /**
     * @var boolean
     */
    public $showEvidences;

    /**
     * @var float
     */
    public $x;

    /**
     * @var float
     */
    public $y;

    /**
     * @param string $placeHolderType
     * @param string $keyword
     * @param int $width
     * @param int $height
     * @param bool $showEvidences
     * @param null|float $x
     * @param null|float $y
     */
    public function __construct($placeHolderType = 'keyword', $keyword, $width, $height, $showEvidences = true, $x = null, float $y = null)
    {
        $this->placeHolderType = $placeHolderType;
        $this->keyword = $keyword;
        $this->width = $width;
        $this->height = $height;
        $this->showEvidences = $showEvidences;
        if ($this->keyword == 'coordinates') {
            $this->x = $x;
            $this->y = $y;
        } else {
            unset($this->x);
            unset($this->y);
        }
    }


}
