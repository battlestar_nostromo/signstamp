<?php

namespace Thiagoprz\SignStamp\Services\Signature\SignatureAddResponse;

use Thiagoprz\SignStamp\Services\Document\DocumentInfo;

class Document
{
    /**
     * @var string
     */
    public $uuid;

    /**
     * @var string
     */
    public $originalFilename;

    /**
     * @var string
     */
    public $documentType;

    /**
     * @var DocumentInfo
     */
    public $documentInfo;

    /**
     * @var string
     */
    public $createdDatetime;
}
