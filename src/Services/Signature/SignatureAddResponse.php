<?php

namespace Thiagoprz\SignStamp\Services\Signature;

use Thiagoprz\SignStamp\Services\Signature\SignatureAddResponse\Document;

/**
 * @package Thiagoprz\SignStamp\Services\Signature;
 */
class SignatureAddResponse
{
    /**
     * @var string
     */
    public $uuid;

    /**
     * @var string
     */
    public $createdDatetime;

    /**
     * @var Document[]
     */
    public $documents;

    /**
     * @var mixed
     */
    public $signatures;

    /**
     * @var int
     */
    public $numberOfDocuments;

    /**
     * @var string
     */
    public $anySignatureInitiated;

    /**
     * @var string
     */
    public $numberOfSignatures;

    /**
     * @var int
     */
    public $numberOfProcessedSignatures;
}
