<?php

namespace Thiagoprz\SignStamp\Services\Signature;

use Thiagoprz\SignStamp\Services\Signature\SignatureStampConfig\AllPagesSettings;

/**
 * @package Thiagoprz\SignStamp\Services\Signature
 */
class SignatureStampConfig
{
    /**
     * @var string
     */
    public $documentUUID;


    /**
     * @var AllPagesSettings|array
     */
    public $allPagesSettings;

    /**
     * @param string $documentUUID
     * @param array|AllPagesSettings $allPagesSettings
     */
    public function __construct(string $documentUUID, $allPagesSettings)
    {
        $this->documentUUID = $documentUUID;
        $this->allPagesSettings = $allPagesSettings;
    }

}
