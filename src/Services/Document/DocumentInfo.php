<?php

namespace Thiagoprz\SignStamp\Services\Document;

class DocumentInfo
{
    /**
     * @var string
     */
    public $title;

    /**
     * @var string
     */
    public $summary;

    /**
     * @param string $title
     * @param string $summary
     */
    public function __construct($title, $summary = '')
    {
        $this->title = $title;
        $this->summary = $summary;
    }
}
