<?php

namespace Thiagoprz\SignStamp\Services\Document;

class DocumentAddResponse
{
    /**
     * @var string
     */
    public $uuid;

    /**
     * @var string
     */
    public $originalFilename;

    /**
     * @var string
     */
    public $documentType;
}
