<?php

namespace Thiagoprz\SignStamp\Services;

use Thiagoprz\SignStamp\Http\Client;

class Jwt
{

    public function authenticate()
    {
        $client = new Client();
        $client->post(config('signstamp.api.v1.auth'), [
            'username'
        ]);
    }
}
