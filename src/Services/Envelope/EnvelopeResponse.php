<?php

namespace Thiagoprz\SignStamp\Services\Envelope;

class EnvelopeResponse
{
    public $uuid;
    public $createdDatetime;
    public $documents;
    public $signatures;
    public $numberOfDocuments;
    public $anySignatureInitiated;
    public $numberOfSignatures;
    public $numberOfProcessedSignatures;
}
