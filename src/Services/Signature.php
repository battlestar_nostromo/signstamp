<?php

namespace Thiagoprz\SignStamp\Services;

use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;
use Thiagoprz\SignStamp\Exception\SignStampErrorException;
use Thiagoprz\SignStamp\Http\Client;
use Thiagoprz\SignStamp\Services\Signature\Signatory;
use Thiagoprz\SignStamp\Services\Signature\SignatureAddResponse;
use Thiagoprz\SignStamp\Services\Signature\SignatureStampConfig;

class Signature
{
    /**
     * @param string $envelopeUuid
     * @param Signatory $signatory
     * @param SignatureStampConfig[] $signatureStampConfig
     * @return SignatureAddResponse
     * @throws SignStampErrorException
     */
    public static function create(string $envelopeUuid, $signatory, array $signatureStampConfig)
    {
        $client = new Client();
        $url = str_replace('{envelope}', $envelopeUuid, config('signstamp.api.v1.signature/add'));
        try {
            return $client->post($url, compact('signatory', 'signatureStampConfig'));
        } catch(ClientException $exception) {
            $message = (string)$exception->getResponse()->getBody();
            $message .= $exception->getMessage();
            throw new SignStampErrorException($message, 500, $url);
        }
    }

    /**
     * @param string $envelopeUuid
     * @param string $signatureUuid
     * @return mixed
     * @throws GuzzleException
     */
    public static function jwt(string $envelopeUuid, string $signatureUuid)
    {
        $client = new Client();
        $url = str_replace(['{envelope}', '{assinatura}'], [$envelopeUuid, $signatureUuid], config('signstamp.api.v1.signature/jwt'));
        $data = $client->get($url);
        return $data->token;
    }


    /**
     * @param string $token
     * @param string $thumbnailImageData
     * @param float $latitude
     * @param float $longitude
     * @param integer $timestamp
     * @param string $ipAddr
     * @param array $customEvidences
     * @return false|mixed
     * @throws GuzzleException
     */
    public static function sign($token, $thumbnailImageData, $latitude = null, $longitude = null, $timestamp = null, $ipAddr = null, $customEvidences = [])
    {
        $client = new Client();
        $data = array_filter(compact('thumbnailImageData', 'latitude', 'longitude', 'timestamp', 'ipAddr', 'customEvidences'));
        $client->setAccessToken("Bearer $token");
        $client->post(config('signstamp.api.v1.signature/sign'), $data);
        return $client->get(config('signstamp.api.v1.signature/status'));
    }

    /**
     * @param $token
     * @return mixed
     * @throws GuzzleException
     */
    public static function status($token)
    {
        $client = new Client();
        $client->setAccessToken("Bearer $token");
        return $client->get(config('signstamp.api.v1.signature/sign'));
    }


    /**
     * @param string $jwt
     * @return \Psr\Http\Message\ResponseInterface
     * @throws GuzzleException
     */
    public static function download(string $jwt)
    {
        $client = new Client();
        $client->setAccessToken("Bearer $jwt");
        $url = config('signstamp.api.baseUrl') . config('signstamp.api.v1.signature/download') . "?jwt=$jwt";
        return file_get_contents($url);
    }
}
