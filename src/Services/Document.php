<?php

namespace Thiagoprz\SignStamp\Services;

use GuzzleHttp\Exception\GuzzleException;
use Thiagoprz\SignStamp\Exception\SignStampErrorException;
use Thiagoprz\SignStamp\Http\Client;
use Thiagoprz\SignStamp\Services\Document\DocumentAddResponse;
use Thiagoprz\SignStamp\Services\Document\DocumentInfo;

class Document
{

    /**
     * @param string $envelopeUUID
     * @param mixed $documentFile
     * @param DocumentInfo $documentInfo
     * @return DocumentAddResponse
     * @throws SignStampErrorException
     */
    public static function create(string $envelopeUUID, $documentFile, $documentInfo)
    {
        $client = new Client();
        $url = str_replace('{envelope}', $envelopeUUID, config('signstamp.api.v1.document/add'));
        try {
            $file = fopen($documentFile, 'r');
            $documentFile = [
                'contents' => $file,
                'name' => 'documentFile',
                'Content-type' => 'multipart/form-data'
            ];
            return $client->upload($url, compact('documentFile'), $documentInfo);
        } catch(GuzzleException $exception) {
            throw new SignStampErrorException($exception->getMessage(), 500, $url);
        }
    }

    /**
     * @param string $envelopeUUID
     * @param string $documentUUID
     * @param string $destiny
     * @return mixed
     * @throws SignStampErrorException
     */
    public static function download(string $envelopeUUID, string $documentUUID, string $destiny)
    {
        $client = new Client();
        $url = config('signstamp.api.v1.document/download');
        $url = str_replace(['{envelope}', '{documento}'], [$envelopeUUID, $documentUUID], $url);
        try {
            $file_path = fopen($destiny,'w');
            return $client->download($url,$file_path);
        } catch(GuzzleException $exception) {
            throw new SignStampErrorException($exception->getMessage(), 500, $url);
        }
    }
}
