<?php

namespace Thiagoprz\SignStamp\Services;

use GuzzleHttp\Exception\GuzzleException;
use Thiagoprz\SignStamp\Exception\SignStampErrorException;
use Thiagoprz\SignStamp\Http\Client;
use Thiagoprz\SignStamp\Services\Envelope\EnvelopeResponse;

/**
 * @package Thiagoprz\SignStamp\Services
 */
class Envelope
{

    /**
     * Cria um envelope
     *
     * @return false|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function create()
    {
        $client = new Client();
        $url = config('signstamp.api.v1.envelope/create');
        try {
            return $client->post($url, []);
        } catch(GuzzleException $exception) {
            throw new SignStampErrorException($exception->getMessage(), 500, $url);
        }
    }

    /**
     * @param mixed $envelopeUUID
     * @return EnvelopeResponse
     * @throws SignStampErrorException
     */
    public static function show($envelopeUUID)
    {
        $client = new Client();
        $url = config('signstamp.api.v1.envelope/show');
        $url = str_replace('{envelope}', $envelopeUUID, $url);
        try {
            return $client->get($url);
        } catch(GuzzleException $exception) {
            throw new SignStampErrorException($exception->getMessage(), 500, $url);
        }
    }
}
