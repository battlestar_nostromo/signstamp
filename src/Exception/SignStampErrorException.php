<?php

namespace Thiagoprz\SignStamp\Exception;

use Throwable;

class SignStampErrorException extends \Exception
{

    /**
     * @var string
     */
    private $url;

    /**
     * @var array
     */
    private $parameters;

    /**
     * @param string $message
     * @param int $code
     * @param string $url
     * @param array $parameters
     */
    public function __construct($message = "", $code = 0, $url = '', $parameters = [])
    {
        $this->url = $url;
        $this->parameters = $parameters;
        parent::__construct($message, $code);
    }

    /**
     * @return array
     */
    public function getParameters(): array
    {
        return $this->parameters;
    }

    /**
     * @param array $parameters
     */
    public function setParameters(array $parameters): void
    {
        $this->parameters = $parameters;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl(string $url): void
    {
        $this->url = $url;
    }
}
