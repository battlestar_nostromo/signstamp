<?php

namespace Thiagoprz\SignStamp\Exception;

use Throwable;

class SafeDelete extends \Exception
{
    /**
     * @param $object
     * @param Throwable|null $previous
     */
    public function __construct($object, Throwable $previous = null)
    {
        parent::__construct(__('signstamp.SafeDelete', ['object' => $object]), 401, $previous);
    }
}
