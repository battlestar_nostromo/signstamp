<?php
return [
    'SafeDelete' => 'Não é possível remover este :object. Existem registros ligados a ele.',
    'envelope' => 'envelope',
    'document' => 'documento',
    'signature' => 'assinatura',
];
