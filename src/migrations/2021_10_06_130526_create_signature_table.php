<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSignatureTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('signstamp_signature', function (Blueprint $table) {
            $table->id();
            $table->string('uuid');
            $table->string('signatory_uid');
            $table->string('name');
            $table->string('email')->nullable();
            $table->string('cellphone')->nullable();
            $table->text('body');
            $table->string('keyword');
            $table->string('envelope_uuid');
            $table->unsignedBigInteger('envelope_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('signstamp_signature');
    }
}
