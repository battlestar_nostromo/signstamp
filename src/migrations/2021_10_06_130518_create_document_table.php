<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDocumentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('signstamp_document', function (Blueprint $table) {
            $table->id();
            $table->string('uuid');
            $table->string('envelope_uuid');
            $table->string('path');
            $table->string('title');
            $table->string('summary')->nullable();
            $table->unsignedBigInteger('envelope_id');
            $table->unsignedBigInteger('signature_id')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('signstamp_document');
    }
}
