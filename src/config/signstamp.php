<?php
return [
    'safe_delete' => env('SIGNSTAMP_SAFE_DELETE', true),
    'debug' => env('APP_DEBUG', false),
    // Desabilita o debug mesmo quando o app estiver com flag de debug
    'disableDebugLog' => env('SIGNSTAMP_DEBUG', false),
    'overwrite' => env('SIGNSTAMP_OVERWRITE', false),
    'api' => [

        'username' => env('SIGNSTAMP_USERNAME'),
        'password' => env('SIGNSTAMP_PASSWORD'),
        'baseUrl' => env('SIGNSTAMP_URL', 'https://pluriosign.com.br/api'),

        // API v1
        'v1' => [
            'auth' => '/v1/autenticar/',
            'jwt/refresh' => '/v1/renovarjwt/',

            // Envelope
            'envelope/create' => '/v1/novoenvelope/',
            'envelope/show' => '/v1/envelope/{envelope}',
            'envelope/status' => '/v1/envelope/{envelope}/status',
            'envelope/download' => '/v1/envelope/{envelope}/download',

            // Document
            'document/add' => '/v1/envelope/{envelope}/adicionardocumento',
            'document/show' => '/v1/envelope/{envelope}/documento/{documento}',
            'document/download' => '/v1/envelope/{envelope}/documento/{documento}/download',

            // Sign
            'signature/add' => '/v1/envelope/{envelope}/adicionarassinatura',
            'signature/show' => '/v1/envelope/{envelope}/assinatura/{assinatura}',
            'signature/jwt' => '/v1/envelope/{envelope}/assinatura/{assinatura}/jwt',
            'signature/sign' => '/v1/assinar',
            'signature/status' => '/v1/status',
            'signature/download' => '/v1/download',
        ],

    ],
];
