<?php

namespace Thiagoprz\SignStamp\Http;

use GuzzleHttp\Client as HttpClient;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

class Client
{

    /**
     * @var HttpClient
     */
    protected $httpClient;

    /**
     * @var string
     */
    private $accessToken;

    /**
     * @var string
     */
    public $log;

    /**
     * Client constructor.
     */
    public function __construct()
    {
        $urlBase = config('signstamp.api.baseUrl');
        $this->httpClient = new HttpClient();
        $this->setLog("Iniciou cliente HTTP. Url base: $urlBase");
    }

    /**
     * @param string $log
     */
    public function setLog(string $log): void
    {
        if (config('signstamp.debug') && !config('signstamp.disableDebugLog')) {
            Log::info($log);
        }
        $this->log .= date('Y-m-d H:i:s - ') . $log . PHP_EOL;
    }

    /**
     *
     */
    protected function authenticate()
    {
        $token = Cache::remember('SIGNSTAMP-TOKEN', 10, function () {
            //$this->setAccessToken('Basic ' . base64_encode(config('signstamp.api.username') . ':' . config('signstamp.api.password')));
            $this->setLog("Realizando autenticação para obter token em " . config('signstamp.api.v1.auth'). ". Autenticando com Basic " . base64_encode(config('signstamp.api.username') . ':' . config('signstamp.api.password')));

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => config('signstamp.api.baseUrl') . config('signstamp.api.v1.auth'),
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_HTTPHEADER => array(
                    'Authorization: Basic ' . base64_encode(config('signstamp.api.username') . ':' . config('signstamp.api.password'))
                ),
            ));

            $jwt = json_decode(curl_exec($curl));

            curl_close($curl);

            if ($jwt->token) {
                $this->setLog("Token obtido: $jwt->token");
                return "Bearer $jwt->token";
            }
        });
        $this->setAccessToken($token);
    }

    /**
     * Request GET
     *
     * @param $url
     * @param array $params
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function get($url, $params = [])
    {
        $this->setLog("GET: $url. Data: " . json_encode($params));
        $result = $this->httpClient->request('GET', config('signstamp.api.baseUrl') . $url, [
            'query' => $params,
            'headers' => $this->headers(),
        ]);
        return json_decode($result->getBody());
    }

    /**
     * Request POST
     *
     * @param string $url
     * @param array $data
     * @param boolean $upload
     * @return false|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function post($url, $data = [], $upload = false)
    {
        $url = config('signstamp.api.baseUrl') . $url;
        $this->setLog("POST: $url. Data: " . json_encode($data));
        $result = $this->httpClient->request('POST', $url, [
            'body' => json_encode($data),
            'headers' => $this->headers($upload),
        ]);
        return json_decode($result->getBody());
    }

    /**
     * Request PUT
     *
     * @param string $url
     * @param array $data
     * @return false|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function put($url, $data = [])
    {
        $url = config('signstamp.api.baseUrl') . $url;
        $this->setLog("PUT: $url. Data: " . json_encode($data));
        $result = $this->httpClient->request('PUT', $url, [
            'body' => json_encode($data),
            'headers' => $this->headers(),
        ]);
        return json_decode($result->getBody());
    }

    /**
     * Request DELETE
     *
     * @param string $url
     * @return false|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function delete($url)
    {
        $url = config('signstamp.api.baseUrl') . $url;
        $result = $this->httpClient->request('DELETE', $url, [
            'headers' => $this->headers(),
        ]);
        return json_decode($result->getBody());
    }

    /**
     * Request UPLOAD
     *
     * @param string $url
     * @param array $files
     * @return false|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function upload($url, $files = [], $formData = [], $method = 'POST')
    {
        $url = config('signstamp.api.baseUrl') . $url;
        $result = $this->httpClient->request($method, $url, [
            'multipart' => $files,
            'form_data' => $formData,
            'headers' => $this->headers(true),
        ]);
        return json_decode($result->getBody());
    }

    /**
     * @param $url
     * @param $stream
     * @return bool
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function download($url, $stream)
    {
        $result = $this->httpClient->get(config('signstamp.api.baseUrl') . $url, [
            'sink' => $stream,
            'headers' => $this->headers(true),
        ]);
        fclose($stream);
        return true;
    }

    /**
     * @return string
     */
    public function getAccessToken(): string
    {
        return $this->accessToken;
    }

    /**
     * @param string $accessToken
     */
    public function setAccessToken(string $accessToken): void
    {
        $this->accessToken = $accessToken;
    }


    public function headers($upload = false)
    {
        if (!$this->accessToken) {
            $this->authenticate();
        }
        $headers = [
            'Authorization' => $this->accessToken,
            'Accept' => 'application/json',
        ];
        if (!$upload) {
            $headers['Content-Type'] = 'application/json';
        }
        return $headers;
    }

    /**
     * @return HttpClient
     */
    public function getHttpClient(): HttpClient
    {
        return $this->httpClient;
    }

}
