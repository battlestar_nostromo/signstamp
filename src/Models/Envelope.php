<?php

namespace Thiagoprz\SignStamp\Models;

use Illuminate\Database\Eloquent\Model;
use Thiagoprz\SignStamp\Exception\SafeDelete;
use Thiagoprz\SignStamp\Services\Envelope as SignStampEnvelope;;


/**
 * @package Thiagoprz\SignStamp\Models
 * @property int $id
 * @property string $uuid
 * @property string $ref
 * @property string $subject
 * @property Document[] $documents
 */
class Envelope extends Model
{

    /**
     * @var string
     */
    protected $table = 'signstamp_envelope';

    /**
     * @var string[]
     */
    protected $fillable = ['uuid', 'ref', 'subject'];

    /**
     * Boot model
     */
    protected static function boot()
    {
        parent::boot();
        self::creating(function(Envelope $envelope) {
            $signStampEnvelope = SignStampEnvelope::create();
            $envelope->uuid = $signStampEnvelope->uuid;
        });
        self::deleting(function(Envelope $envelope) {
            if (config('signstamp.safe_delete') && $envelope->documents()->count() > 0) {
                throw new SafeDelete(__('signstamp.envelope'));
            }
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function documents()
    {
        return $this->hasMany(Document::class);
    }

}
