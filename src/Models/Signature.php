<?php

namespace Thiagoprz\SignStamp\Models;

use Illuminate\Database\Eloquent\Model;
use Thiagoprz\SignStamp\Services\Signature as SignStampSignature;
use Thiagoprz\SignStamp\Services\Signature\SignatureAddResponse;

/**
 * @package Thiagoprz\SignStamp\Models
 * @property int $id
 * @property string $uuid
 * @property string $signatory_uid
 * @property string $name
 * @property string $email
 * @property string $cellphone
 * @property int $envelope_id
 * @property string $body
 * @property string $keyword
 * @property string $envelope_uuid
 * @property string $png
 * @property string $token
 * @property string $status
 */
class Signature extends Model
{

    /**
     * @var string
     */
    protected $table = 'signstamp_signature';

    /**
     * @var string[]
     */
    protected $fillable = ['uuid', 'signatory_uid', 'name', 'email', 'cellphone', 'body', 'keyword', 'envelope_uuid', 'envelope_id', 'png', 'token', 'status'];


    protected $hidden = ['keyword'];

    /**
     * @var Document[]
     */
    protected $documentsAdded;


    /**
     * Boot model
     */
    protected static function boot()
    {
        parent::boot();
        self::creating(function(Signature $signature) {
            $envelope = Envelope::whereUuid($signature->envelope_uuid)->first();
            $signature->envelope_id = $envelope->id;
            $signatory = new SignStampSignature\Signatory($signature->signatory_uid, $signature->name, $signature->email, $signature->cellphone, $signature->keyword, 0);

            $signatureStampConfig = [];
            foreach ($signature->documentsAdded as $document) {
                $signatureStampConfig[] = new SignStampSignature\SignatureStampConfig($document->uuid, new SignStampSignature\SignatureStampConfig\AllPagesSettings('keyword', $signature->keyword, 200, 40));
            }

            $signStampSignature = SignStampSignature::create($signature->envelope_uuid, $signatory, $signatureStampConfig);
            $signature->uuid = $signStampSignature->uuid;
            $signature->body = json_encode($signStampSignature);
            $signature->png = $signature->generatePNG($signature->name);
            $signature->token = \Thiagoprz\SignStamp\Services\Signature::jwt($signature->envelope_uuid, $signature->uuid);
            $signature->status = \Thiagoprz\SignStamp\Services\Signature::sign($signature->token, $signature->png)->signatureStatus;
        });
    }

    /**
     * @param $body
     * @return SignatureAddResponse|mixed
     */
    public function getBodyAttribute($body)
    {
        return json_decode($body);
    }

    /**
     * @return Document[]
     */
    public function getDocumentsAdded(): array
    {
        return $this->documentsAdded;
    }

    /**
     * @param Document[] $documentsAdded
     */
    public function addDocuments(array $documentsAdded): void
    {
        $this->documentsAdded = $documentsAdded;
    }

    private function generatePNG($text)
    {
        $font = 25;
        $string = ' ';
        $im = @imagecreatetruecolor(strlen($string) * $font / 1.5, 5);
        imagesavealpha($im, true);
        imagealphablending($im, false);
        $white = imagecolorallocatealpha($im, 255, 255, 255, 127);
        imagefill($im, 0, 0, $white);
        $black = imagecolorallocate($im, 0, 0, 0);
        imagestring($im, $font, 0, 0, $string, $black);
        header("Content-type: image/png");
        ob_start ();
        imagepng($im);
        $image_data = ob_get_contents ();
        ob_end_clean ();
        return base64_encode($image_data);
    }
}
