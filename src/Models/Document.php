<?php

namespace Thiagoprz\SignStamp\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Thiagoprz\SignStamp\Services\Document as SignStampDocument;

/**
 * @package Thiagoprz\SignStamp\Models
 * @property int $id
 * @property string $uuid
 * @property string $envelope_uuid
 * @property string $path
 * @property string $title
 * @property string $summary
 * @property int $envelope_id
 * @property Envelope $envelope
 */
class Document extends Model
{

    /**
     * @var string
     */
    protected $table = 'signstamp_document';

    /**
     * @var string[]
     */
    protected $fillable = ['uuid', 'envelope_id', 'envelope_uuid', 'path', 'title', 'summary'];

    /**
     * Boot model
     */
    protected static function boot()
    {
        parent::boot();
        self::creating(function(Document $document) {
            $signStampDocument = SignStampDocument::create($document->envelope_uuid, $document->path, ['title' => $document->title, 'summary' => $document->summary]);
            $document->uuid = $signStampDocument->uuid;
            $document->envelope_id = Envelope::whereUuid($document->envelope_uuid)->first()->id;
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function envelope()
    {
        return $this->belongsTo(Envelope::class);
    }

}
